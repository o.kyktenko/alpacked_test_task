#Set the base image Ubuntu 18.04 Bionic
FROM ubuntu:bionic

#Set global environment variable DEBIAN_FRONTEND with value noninteractive for zero interaction while installing or upgrading the system via apt.
ENV DEBIAN_FRONTEND=noninteractive

#Copy dfh.sh script for supervisor, this script help supervisor start a daemon and runs in foreground (zabbix_server, zabbix_agentd, zabbix-java-gateway). 
COPY dfg.sh /usr/local/bin/dfg.sh

#Installing wget, downloading zabbix-release_4.0-3+bionic_all.deb for seting zabbix repo.
RUN apt-get update && apt-get install wget -y && \
    wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+bionic_all.deb && \
    dpkg -i /zabbix-release_4.0-3+bionic_all.deb && \ 
    apt-get -y update && apt-get upgrade -y && \
#installing zabbix server, web interface and agent.
    apt-get -y install zabbix-server-mysql zabbix-frontend-php zabbix-agent && \
#Creating directory for supervisor.
    mkdir /var/run/supervisor && \
    apt-get -y update && \
#Changing folders permitions to read, write, & execute for owner, group and others.
    chmod -R 0777  /etc/zabbix && \
    mkdir /var/run/zabbix && \
    chmod -R 0777 /var/run/zabbix 
#Copy file create.sql.gz from host into docker for import the initial schema and data.
COPY create.sql.gz /usr/share/doc/zabbix-server-mysql/create.sql.gz

#Starting mysql, seting initial configuration. 
RUN /bin/bash -c "/usr/bin/mysqld_safe &" && \
    sleep 5 && \
    mysql -e "create user 'zabbix'@'localhost';" && \
    mysql -e "create database zabbix character set utf8 collate utf8_bin;" && \
    mysql -e "grant all privileges on zabbix.* to 'zabbix'@'localhost';" && \
    mysql -e "flush privileges;" && \
    cd /usr/share/doc/zabbix-server-mysql && zcat create.sql.gz | mysql -uroot zabbix 
#Installing debuging tools and supervisor.
RUN apt-get install -y net-tools && \
    apt-get install -y supervisor


COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

#Copy custom config file for web interface.
COPY zabbix.conf /etc/apache2/conf-available/zabbix.conf

EXPOSE 10051 22 80 3306

#Execute all processes in supervisord.conf (zabbix_server, zabbix_agentd, zabbix-java-gateway, mysql, apache2)
CMD ["/usr/bin/supervisord"]
