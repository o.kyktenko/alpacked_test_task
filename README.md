About 
=========
Dockerfile build image with Zabbix Server 4.0 and based on Ubuntu 18.04 (bionic) and using Supervisord as process/service control system.

Using Dockerfile you will build an image with:

 - MySQL DB 
 - Apache server with listening 80 port
 - net-tools for debuging
 - zabbix_server and zabbix_agentd
 - Supervisor

Requirements
------------

 Dockerfile require all files in repository:

 - dfg.sh (script for supervisor, helps the supervisor start and runs in foreground)
 - supervisord.conf (configuration file for supervisor, describes processes for start zabbix_server, zabbix_agentd, zabbix-java-gateway, mysql, apache2 )
 - zabbix.conf (custom config file for web interface)
 - create.sql.gz (the archive that imports initial schema and data)

Run
----------------
You can pull the image: 

	docker pull okyktenko/zabbix:latest

or clone the repo to the host https://gitlab.com/o.kyktenko/alpacked_test_task.git,
and build image 

        docker build -t <image tag> <path to Dockerfile>

Then run docker container: 

        docker run -d -p 0.0.0.0:8080:80 <image tag>


Then open Zabbix frontend: http://server_ip/zabbix
Follow the steps in this instruction: https://www.zabbix.com/documentation/4.0/ru/manual/installation/install#installing_frontend

License
-------

BSD-3-Clause (default)

Author Information
------------------

Oleksandr Kyktenko 
